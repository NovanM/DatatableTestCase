<?php

namespace App\Http\Controllers;

use App\Models\T_Candidate;
use Illuminate\Http\Request;
use Validator;

class TCandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  

        if (request()->ajax()) {
            return datatables()->of(T_Candidate::latest())
                ->addColumn('action', function($data){
                    $button = '<button type="button" name="edit" id="'.$data->candidate_id.'" class="edit btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModal">Edit</button>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<button type="button" name="delete" id="'.$data->candidate_id.'" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view(
            'candidate.index'
        );
    }
    public function home()
    {
        return view('candidate.index');
    }


  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreT_CandidateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $rules  = [
            'full_name' =>'required|string',
            'dob'=>'required',
            'pob'=>'required',
            'gender'=>'required|in:F,M',
            'year_exp'=>'required',
        ];
        $text = [
            'full_name.required' => 'Field Name Required',
            'dob.required' => 'Field Date of Birth Required',
            'pob.required' => 'Field Place of Birth Required',
            'gender.required' => 'Field Gender Required',
            'year_exp.required'=> 'Field Year Experience Required',
        ];
        $error = Validator::make($request->all(), $rules,$text);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $data = new T_Candidate();
        $data->full_name = $request->full_name;
        $data->dob = $request->dob;
        $data->pob = $request->pob;
        $data->gender = $request->gender;
        $data->year_exp = $request->year_exp;
        $data->last_salary = $request->last_salary;
        $simpan = $data->save();
        if ($simpan) {
            return response()->json(['text' => 'Data Succesfuly Added'], 200);
        } else {
            return response()->json(['text' => 'Data Failed Please Check Your input'],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\T_Candidate  $t_Candidate
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        # code...
        $candidate_id = $request->candidate_id;
        $data = T_Candidate::find($candidate_id);
        return response()->json(['data' => $data]);
    }
    /**
     * Ubah Data
     *
     * @param  mixed $var
     * @return void
     */
    public function update(Request $request)
    {
        // dd($request->all());
        $candidate_id = $request->candidate_id;
        $datas = [
            'full_name' => $request->full_name,
            'dob' => $request->dob,
            'pob' => $request->pob,
            'gender' => $request->gender,
            'year_exp' => $request->year_exp,
            'last_salary' => $request->last_salary,
        ];
        $data = T_Candidate::find($candidate_id);
        $simpan = $data->update($datas);
        if ($simpan) {
            return response()->json(['text' => 'Data Updated'], 200);
        } else {
            return response()->json(['text' => 'Failed to Update'], 422);
        }
    }

    /**
     * hapus
     *
     * @param  mixed $request
     * @return void
     */
    public function destroy(Request $request)
    {
        $candidate_id = $request->candidate_id;
        $data = T_Candidate::find($candidate_id);
        $data->delete();
        return response()->json(['text' => 'Data Deleted'], 200);
    }
}
