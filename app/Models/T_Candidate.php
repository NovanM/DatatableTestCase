<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_Candidate extends Model
{
    use HasFactory;
    protected $fillable = ["full_name","dob","pob","gender","year_exp","last_salary"];
    protected $hidden = ["created_at","updated_at"];
    protected $primaryKey = "candidate_id";
}
