<?php

use App\Http\Controllers\TCandidateController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
    return redirect()->route('candidate.index');
});


Route::get('/home-candidate', [App\Http\Controllers\TCandidateController::class, 'home'])->name('home-candidate');
Route::get('/candidate', [TCandidateController::class, 'index'])->name('candidate.index');

Route::post('candidate.store', [TCandidateController::class, 'store'])->name('candidate.store');
Route::post('candidate.edit', [TCandidateController::class, 'edit'])->name('candidate.edit');
Route::post('candidate.update', [TCandidateController::class, 'update'])->name('candidate.update');
Route::post('candidate.destroy', [TCandidateController::class, 'destroy'])->name('candidate.destroy');

