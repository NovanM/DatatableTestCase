<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t__candidates', function (Blueprint $table) {
            $table->bigIncrements("candidate_id");
            $table->string("full_name",50);
            $table->string("dob",20);
            $table->string("pob",50);
            $table->enum("gender",["F","M"]);
            $table->string("year_exp",11);
            $table->string("last_salary",50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t__candidates');
    }
}
