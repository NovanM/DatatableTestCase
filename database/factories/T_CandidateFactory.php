<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


class T_CandidateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'full_name' => $this->faker->name(),
            'dob' =>  $this->faker->dateTimeBetween($startDate = '-20 years', $endDate = 'now', $timezone = null),
            'pob' => $this->faker->city(),
            'gender' =>$this->faker->randomElement(['M', 'F']),
            'year_exp'=> rand(0,20),
            'last_salary' => $this->faker->randomNumber($nbDigits = 3), 
        ];
    }
}
