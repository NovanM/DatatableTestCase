<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Datatables TestCase1</title>
    @include('layouts.style')
   

</head>

<body>
    <div class="container">
        <div class="col-md-12">

            <div class="card-header">
                <button type="button" class="btn btn-primary my-3" data-bs-toggle="modal"
                    data-bs-target="#exampleModal">
                    Add
                </button>

                <strong class="card-title" style="margin-top: 10px">Data Candidate</strong>
            </div>

            <div class="read">
                <div class="card-body">
                    <table class="table table-striped table-bordered" id="datatable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Day Of Birth</th>
                                <th>Place Of Birth</th>
                                <th>Gender</th>
                                <th>Experience</th>
                                <th>Salary</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
             
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <h4 class="modal-title" id="modelHeading"></h4>

                </div>
                <span id="form_result"></span>
                <div class="modal-body">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="" class="col-md-4">
                                Name
                            </label>
                            <input type="text" class="form-control form-control-user" id="full_name" name="full_name"
                                placeholder="Name">

                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4">
                                Date Of Birth
                            </label>
                            <input type="date" class="form-control form-control-user" id="dob" name="dob">

                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4">
                                Place Of Birth
                            </label>
                            <input type="text" class="form-control form-control-user" id="pob" name="pob"
                                placeholder="place">

                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4">
                                Gender
                            </label>

                            <select class="form-control" name="gender" id="gender">
                                <option value="" label="Gender"></option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                            </select>

                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4">
                                Year Experience
                            </label>
                            <input type="number" class="form-control form-control-user" id="year_exp" name="year_exp"
                                placeholder="1">

                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4">
                                Last Salary
                            </label>
                            <input type="text" class="form-control form-control-user" id="last_salary"
                                name="last_salary" placeholder="$1">

                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="tutup" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" id="save" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>



    @include('layouts.script')
    <script>
    $(document).ready(function() {

        read();
    });

    function read() {
        $('#datatable').DataTable({
            responsive:true,
            serverSide:true,
            "processing": true,
            ajax:{
                url:"{{route('candidate.index')}}"
            },
            columns:[
              {  
                data:'full_name',
                name:'full_name',
                },
                {
                    data:'dob',
                    name:'dob',
                },
                {
                    data:'pob',
                    name:'pob',
                },
                {
                data:'gender',
                name:'gender',
                },
                {
                    data:'year_exp',
                    name:'year_exp',
                },
                {
                data:'last_salary',
                name:'last_salary',
                },
                {
                data:'action',
                name:'action',
                orderlable:false,
                },

            ]
        });
    }

    
</script>
<script>
    $('#save').on('click',function () {
    if ($(this).text() === 'Save Edit') {
        // console.log('Edit');
       edit()
    } else {
      store()
    }


})

$(document).on('click','.edit', function () {
    let candidate_id = $(this).attr('id')
    console.log(candidate_id);
    $('#save').text('Save Edit')
    $('#form_result').html('');
    $.ajax({
        url : "{{route('candidate.edit')}}",
        type : 'post',
        data : {
            candidate_id : candidate_id,
            _token : "{{csrf_token()}}"
        },
        success: function (res) {
                console.info();
                $('#full_name').val(res.data.full_name);
                $('#dob').val(res.data.dob.split(' ')[0]);
                $('#pob').val(res.data.pob);
                $('#gender').val(res.data.gender);
                $('#year_exp').val(res.data.year_exp);
                $('#last_salary').val(res.data.last_salary);
        }
    })

})

function store() {
    
    $.ajax({
            
            url : "{{route('candidate.store')}}",
            type : "post",
            data : {
                full_name : $('#full_name').val(),
                dob : $('#dob').val(),
                pob : $('#pob').val(),
                gender : $('#gender').val(),
                year_exp : $('#year_exp').val(),
                last_salary : $('#last_salary').val(),
                "_token" : "{{csrf_token()}}"
            },
            success : function (res) {
                var html = '';
                if (res.errors) {
                html = '<div class="alert alert-danger">';
                for(var count = 0; count < res.errors.length; count++){
                html += '<p>' + res.errors[count] + '</p>';
                }
                html += '</div>';
                }
                else
                {
                console.log(res);
                alert(res.text)
                $('#tutup').click()
                $('#datatable').DataTable().ajax.reload()
                $('#full_name').val(null)
                $('#dob').val(null)
                $('#pob').val(null)
                $('#gender').val(null)
                $('#year_exp').val(null)
                $('#last_salary').val(null)
                }
                $('#form_result').html(html);
                

            },
            error : function (xhr) {
                
                alert(xhr.responJson.text)
            }
        })
}

function edit() {
    let candidate_id = $(".edit").attr('id');
    console.info(candidate_id);
    $.ajax({
            url : "{{route('candidate.update')}}",
            type : "post",
            data : {
                candidate_id:candidate_id,
                full_name : $('#full_name').val(),
                dob : $('#dob').val(),
                pob : $('#pob').val(),
                gender : $('#gender').val(),
                year_exp : $('#year_exp').val(),
                last_salary : $('#last_salary').val(),
                "_token" : "{{csrf_token()}}"
            },
            success : function (res) {
                console.log(res);
                alert(res.text)
                $('#tutup').click()
                $('#datatable').DataTable().ajax.reload()
                $('#full_name').val(null)
                $('#dob').val(null)
                $('#pob').val(null)
                $('#gender').val(null)
                $('#year_exp').val(null)
                $('#last_salary').val(null)
            },
            error : function (xhr) {
                alert(xhr.responJson.text)
            }
        }) 
}

$(document).on('click','.delete', function () {
    let candidate_id = $(this).attr('id')
    console.info(candidate_id);
    $.ajax({
        url : "{{route('candidate.destroy')}}",
        type : 'post',
        data: {
            candidate_id: candidate_id,
            "_token" : "{{csrf_token()}}"
        },
        success: function (params) {
            alert(params.text)
            $('#datatable').DataTable().ajax.reload()
        }
    })
})
</script>


</body>

</html>